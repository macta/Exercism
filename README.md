Spike for exercism in Pharo

Install Exercism and run 

```
exercism fetch pharo
```

You then need to install Pharo using [zero conf](http://pharo.org/download#//*[@id="main"]/div/h2[3]) from a terminal command line.

To do this, type:
```$xslt
cd pharo
curl https://get.pharo.org | bash
```

Then finally launch Pharo and install some helper tools by typing:
```$xslt
./pharo-ui Pharo.image eval "Metacello new 
baseline: 'Exercism'; 
repository: 'gitlab://macta/Exercism:master/src'; 
load.
(RPackageOrganizer default packageNamed: 'Exercism') browse"
```

You will see the Exercism tools load into Pharo, and then you are presented with a coding System Browser ready to start