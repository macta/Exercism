Class {
	#name : #HelloWorld,
	#superclass : #Object,
	#category : #'Exercism-HelloWorld'
}

{ #category : #exercism }
HelloWorld class >> sayHello [

	^'Hello, World!'
]
